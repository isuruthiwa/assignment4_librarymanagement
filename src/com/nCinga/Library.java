package com.nCinga;

public class Library {
    static Book[] books;
    static Admin admin;
    int totalBooks;
    int totalIssuedBooks;

    Library(Admin admin){
        this.admin=admin;
    }


    public static void main(String[] args) {
        books = new Book[100];
        books[0]=new Book("Engineering Mathematics",Subject.MATHS,5,2);
        books[1]=new Book("Electromagnetic",Subject.PHYSICS,5,3);
        books[2]=new Book("Electronic Materials",Subject.CHEMISTRY,7,3);
        books[3]=new Book("Industrial Processes",Subject.CHEMISTRY,5,5);
        books[4]=new Book("Calculus",Subject.MATHS,2,0);
        books[5]=new Book("Dynamic Motion",Subject.PHYSICS,1,0);
        books[6]=new Book("Static Motion",Subject.PHYSICS,3,2);

        Student student = new Student(1,"Kamal","2020","CSE",Degree.B_TECH);

        admin = new Admin("Fernando");
        admin.issueBook(books[0],student);

        student.returnBook(books[4]);

    }
}
