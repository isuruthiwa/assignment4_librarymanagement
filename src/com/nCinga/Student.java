package com.nCinga;

public class Student {
    int rollNo;
    String name;
    String batch;
    String section;
    Degree degree;

    void returnBook(Book book){
        book.availableCount++;
    }

    Student(int rollNo,String name, String batch,String section, Degree degree){
        this.rollNo=rollNo;
        this.name=name;
        this.batch=batch;
        this.section=section;
        this.degree=degree;
    }

    public Degree getDegree() {
        return degree;
    }


}
