package com.nCinga.Exceptions;

public class BookOutOfBounds extends RuntimeException {

    public BookOutOfBounds(String msg){
        super(msg);
    }
}
