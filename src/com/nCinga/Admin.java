package com.nCinga;

import com.nCinga.Exceptions.BookOutOfBounds;

import static com.nCinga.Library.books;

public class Admin {
    String name;
    static int adminId;

    void issueBook(Book book, Student student){
        if(book.availableCount>0) {
            book.availableCount--;
            if(student.getDegree()==Degree.B_TECH){

            }
            else if(student.getDegree()==Degree.M_TECH){

            }
        }
        else
            throw new BookOutOfBounds("No available book");

    }

    Admin(String name){
        this.name=name;
        adminId++;
    }
}
