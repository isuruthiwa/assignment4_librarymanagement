package com.nCinga;

public class Book {
    String name;
    Subject subject;
    int availableCount;
    int totalBookCount;

    Book(String name, Subject subject, int totalBookCount, int availableCount){
        this.name = name;
        this.subject =subject;
        this.totalBookCount =totalBookCount;
        this.availableCount =availableCount;
    }

    public int getAvailableCount() {
        return availableCount;
    }

    public int getTotalBookCount() {
        return totalBookCount;
    }

    public void setAvailableCount(int availableCount) {
        this.availableCount = availableCount;
    }
}
